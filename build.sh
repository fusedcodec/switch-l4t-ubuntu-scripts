#!/bin/bash

set -e

function get_abs_filename {
  # $1 : relative filename
  echo "$(cd "$(dirname "$1")" && pwd)/$(basename "$1")"
}

function show_usage {
	echo "Usage: $1 [<rootfs>] [<driver package>] [<config package>] [<output image>]"
cat <<EOF
	This script creates a sdcard image of ubuntu for the nintendo switch.
	If no config package use a - in place.
EOF
}

export LC_ALL=C

PACKAGES="openbox obconf pulseaudio-module-bluetooth onboard nintendo-switch-meta"
SCRIPT_NAME=$(basename $0)
SCRIPT_PATH=$(realpath -s $0)
SCRIPT_DIR=$(dirname $SCRIPT_PATH)

if [ $# != 4 ]; then
	show_usage $SCRIPT_NAME
	exit 1
fi

ROOTFS=$(get_abs_filename "$1")
DRIVERS=$(get_abs_filename "$2")
CONFIG=$(get_abs_filename "$3")
OUTPUT=$(get_abs_filename "$4")

if [ ! -f "$ROOTFS" ]; then
    echo "Rootfs not found!"
	exit 1
fi


if [ ! -f "$DRIVERS" ]; then
    echo "Driver package not found!"
	exit 1
fi

if [ ! -f "$CONFIG" ]; then
    echo "Switch config package not found, skipping"
fi

if [ ! -x "$(command -v virt-make-fs)" ]; then
  echo "virt-make-fs not found!"
  exit 1
fi

if [ -f "$OUTPUT" ]; then
    echo "Output image already exists!"
	exit 1
fi

echo "Creating image at $OUTPUT"

cd /tmp

if [ -d /tmp/l4t-builder-temp ]; then
	if [ -f /tmp/l4t-builder-temp/Linux_for_Tegra/rootfs/dev/null ]; then
		sudo umount l4t-builder-temp/Linux_for_Tegra/rootfs/dev/pts
		sudo umount l4t-builder-temp/Linux_for_Tegra/rootfs/dev
		sudo umount l4t-builder-temp/Linux_for_Tegra/rootfs/sys
		sudo umount l4t-builder-temp/Linux_for_Tegra/rootfs/proc
	fi

	echo "Removing old temporary files"
	sudo rm -rf l4t-builder-temp
	echo "Done!"
fi

mkdir l4t-builder-temp
cd l4t-builder-temp

echo "Extracting driver package"
sudo tar -xpf "$DRIVERS"
echo "Done!"

if [ -f "/tmp/l4t-builder-temp/Linux_for_Tegra/rootfs" ]; then
	echo "Invalid driver package"
	exit 1
fi

cd Linux_for_Tegra/rootfs

INSTALLER_CHECKSUM="$(md5sum ../apply_binaries.sh | awk '{ print $1 }')"

echo "Extracting rootfs"
sudo tar -xpf "$ROOTFS"
echo "Done!"

cd ..

echo "Copying host resolv.conf to rootfs"
sudo mv rootfs/etc/resolv.conf ./
sudo cp /etc/resolv.conf rootfs/etc/resolv.conf
echo "Done!"

echo "Updating apt repos in rootfs"
sudo sed -i 's/http:\/\/ports\.ubuntu\.com\/ubuntu-ports\//http:\/\/turul.canonical.com\//g' rootfs/etc/apt/sources.list
echo "Done!"

echo "Preparing chroot environment"
sudo mount -o bind /dev rootfs/dev
sudo mount -t sysfs none rootfs/sys
sudo mount -t proc none rootfs/proc
sudo mount -t devpts none rootfs/dev/pts
echo "Done!"

echo "Updating apt repositories"
cat << EOF | sudo chroot rootfs
apt update
EOF
echo "Done!"


echo "Installing desktop packages"
cat << EOF | sudo chroot rootfs
chown root:root /
yes | unminimize
EOF
echo "Done!"

echo "Adding switchroot key"
cat << EOF | sudo chroot rootfs
wget https://newrepo.switchroot.org/pubkey
apt-key add pubkey
rm pubkey
EOF
echo "Done!"

echo "Adding switchroot repo"
cat << EOF | sudo chroot rootfs
export DEBIAN_FRONTEND=noninteractive
wget https://newrepo.switchroot.org/pool/unstable/s/switchroot-newrepo/switchroot-newrepo_1.1_all.deb
dpkg -i switchroot-newrepo_1.1_all.deb
rm switchroot-newrepo_1.1_all.deb
EOF
echo "Done!"

echo "Updating apt repositories"
cat << EOF | sudo chroot rootfs
apt update
EOF
echo "Done!"

echo "Installing switch configuration packages"
cat << EOF | sudo chroot rootfs
export DEBIAN_FRONTEND=noninteractive
apt -y install $PACKAGES
EOF
echo "Done!"

echo "Unpreparing chroot environment"
sudo umount rootfs/dev/pts
sudo umount rootfs/dev
sudo umount rootfs/sys
sudo umount rootfs/proc
echo "Done!"

echo "Copying back old resolv.conf"
sudo mv ./resolv.conf rootfs/etc/resolv.conf
echo "Done!"

echo "Backing up default bluetooth config"
sudo cp rootfs/lib/systemd/system/bluetooth.service .
echo "Done!"

echo "Installing driver package"
sudo ./apply_binaries.sh
echo "Done!"

echo "Restoring up default bluetooth config"
sudo mv ./bluetooth.service rootfs/lib/systemd/system/ -f
echo "Done!"

cd rootfs

echo "Patching ubiquity"
sudo mkdir -p usr/lib/ubiquity/dm-scripts/oem
sudo cp $SCRIPT_DIR/files/ubiquity-oem-script usr/lib/ubiquity/dm-scripts/oem/switch-randr
sudo patch usr/bin/ubiquity-dm $SCRIPT_DIR/files/ubiquity-force-enable-onboard.patch
sudo patch usr/bin/ubiquity-dm $SCRIPT_DIR/files/ubiquity-use-openbox.patch
sudo patch usr/sbin/oem-config-remove-gtk $SCRIPT_DIR/files/oem-config-uninstall-openbox.patch
echo "Done!"

echo "Applying switchroot customizations"
sudo patch etc/xdg/autostart/nvbackground.sh $SCRIPT_DIR/files/nvbackground.patch
sudo cp $SCRIPT_DIR/files/Switchroot_Wallpaper.png usr/share/backgrounds/
sudo rm -f etc/skel/Desktop/*
echo "Done"

echo "Installing display script"
sudo cp $SCRIPT_DIR/files/nintendo-switch-display.desktop usr/share/gdm/greeter/autostart/
sudo cp $SCRIPT_DIR/files/nintendo-switch-display.desktop etc/xdg/autostart/
echo "Done!"

cd ..

if [ -f "$CONFIG" ]; then
	echo "Extracting config package"
	cd rootfs
	sudo tar -xpf "$CONFIG"
	cd ..
	echo "Done!"
fi

sudo virt-make-fs --size=+200M --format=raw --type=ext4 rootfs "$OUTPUT"

echo "Successfully built $OUTPUT"
